package ru.tsc.ichaplygina.taskmanager.model;

import ru.tsc.ichaplygina.taskmanager.util.NumberUtil;

public abstract class AbstractModel {

    private final String id = NumberUtil.generateId();

    public String getId() {
        return id;
    }

}
