package ru.tsc.ichaplygina.taskmanager.service;

import ru.tsc.ichaplygina.taskmanager.api.repository.IRepository;
import ru.tsc.ichaplygina.taskmanager.api.service.IAuthService;
import ru.tsc.ichaplygina.taskmanager.api.service.IProjectService;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.NameEmptyException;
import ru.tsc.ichaplygina.taskmanager.model.Project;

import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;

public class ProjectService extends AbstractEntityService<Project> implements IProjectService {

    public ProjectService(IRepository<Project> repository, IAuthService authService) {
        super(repository, authService);
    }

    @Override
    public Project add(final String name, final String description) throws AbstractException {
        if (isEmptyString(name)) throw new NameEmptyException();
        final Project project = new Project(name, description, authService.getCurrentUserId());
        return super.add(project);
    }

}
