package ru.tsc.ichaplygina.taskmanager.service;

import ru.tsc.ichaplygina.taskmanager.api.repository.IProjectRepository;
import ru.tsc.ichaplygina.taskmanager.api.repository.ITaskRepository;
import ru.tsc.ichaplygina.taskmanager.api.service.IAuthService;
import ru.tsc.ichaplygina.taskmanager.api.service.IProjectTaskService;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.NameEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.ProjectNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.TaskNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IndexIncorrectException;
import ru.tsc.ichaplygina.taskmanager.model.Project;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.Comparator;
import java.util.List;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.EMPTY;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isInvalidListIndex;

public class ProjectTaskService implements IProjectTaskService {

    private final ITaskRepository taskRepository;

    private final IProjectRepository projectRepository;

    private final IAuthService authService;

    public ProjectTaskService(final ITaskRepository taskRepository, final IProjectRepository projectRepository, final IAuthService authService) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
        this.authService = authService;
    }

    private void addTaskToProjectForAdmin(final String projectId, final String taskId) throws AbstractException {
        if (taskRepository.isNotFoundById(taskId)) throw new TaskNotFoundException();
        if (projectRepository.isNotFoundById(projectId)) throw new ProjectNotFoundException();
        taskRepository.updateProjectId(taskId, projectId);
    }

    private void addTaskToProjectForUser(final String projectId, final String taskId, final String userId) throws AbstractException {
        if (taskRepository.isNotFoundByIdForUser(taskId, userId)) throw new TaskNotFoundException();
        if (projectRepository.isNotFoundByIdForUser(projectId, userId)) throw new ProjectNotFoundException();
        taskRepository.updateProjectId(taskId, projectId);
    }

    private void removeTaskFromProjectForAdmin(final String projectId, final String taskId) throws AbstractException {
        if (taskRepository.isNotFoundById(taskId)) throw new TaskNotFoundException();
        if (projectRepository.isNotFoundById(projectId)) throw new ProjectNotFoundException();
        if (taskRepository.isNotFoundTaskInProject(taskId, projectId)) throw new TaskNotFoundException();
        taskRepository.updateProjectId(taskId, EMPTY);
    }

    private void removeTaskFromProjectForUser(final String projectId, final String taskId, final String userId) throws AbstractException {
        if (taskRepository.isNotFoundByIdForUser(taskId, userId)) throw new TaskNotFoundException();
        if (projectRepository.isNotFoundByIdForUser(projectId, userId)) throw new ProjectNotFoundException();
        if (taskRepository.isNotFoundTaskInProject(taskId, projectId)) throw new TaskNotFoundException();
        taskRepository.updateProjectId(taskId, EMPTY);
    }

    private List<Task> findAllTasksByProjectIdForAdmin(final String projectId, final Comparator<Task> taskComparator) {
        if (taskComparator == null) return taskRepository.findAllByProjectId(projectId);
        return taskRepository.findAllByProjectId(projectId, taskComparator);
    }

    private List<Task> findAllTasksByProjectIdForUser(final String projectId, final Comparator<Task> taskComparator, final String userId) {
        if (taskComparator == null) return taskRepository.findAllByProjectIdForUser(projectId, userId);
        return taskRepository.findAllByProjectIdForUser(projectId, taskComparator, userId);
    }

    private Project removeProjectByIdForAdmin(final String projectId) {
        taskRepository.removeAllByProjectId(projectId);
        return projectRepository.removeById(projectId);
    }

    private Project removeProjectByIdForUser(final String projectId, final String userId) throws AbstractException {
        if (projectRepository.isNotFoundByIdForUser(projectId, userId)) throw new ProjectNotFoundException();
        taskRepository.removeAllByProjectId(projectId);
        return projectRepository.removeByIdForUser(projectId, userId);
    }

    private Project removeProjectByIndexForAdmin(final int projectIndex) throws AbstractException {
        if (isInvalidListIndex(projectIndex, projectRepository.getSize()))
            throw new IndexIncorrectException(projectIndex + 1);
        final String projectId = projectRepository.getId(projectIndex);
        return removeProjectById(projectId);
    }

    private Project removeProjectByIndexForUser(final int projectIndex, final String userId) throws AbstractException {
        if (isInvalidListIndex(projectIndex, projectRepository.getSizeForUser(userId)))
            throw new IndexIncorrectException(projectIndex + 1);
        final String projectId = projectRepository.getIdForUser(projectIndex, userId);
        return removeProjectById(projectId);
    }

    private Project removeProjectByNameForAdmin(final String projectName) throws AbstractException {
        final String projectId = projectRepository.getId(projectName);
        if (projectId == null) throw new ProjectNotFoundException();
        return removeProjectById(projectId);
    }

    private Project removeProjectByNameForUser(final String projectName, final String userId) throws AbstractException {
        final String projectId = projectRepository.getIdForUser(projectName, userId);
        if (projectId == null) throw new ProjectNotFoundException();
        return removeProjectById(projectId);
    }

    private void clearProjectsForAdmin() {
        for (final Project project : projectRepository.findAll())
            taskRepository.removeAllByProjectId(project.getId());
        projectRepository.clear();
    }

    private void clearProjectsForUser(final String userId) {
        for (final Project project : projectRepository.findAllForUser(userId))
            taskRepository.removeAllByProjectId(project.getId());
        projectRepository.clearForUser(userId);
    }

    @Override
    public int getProjectSize() throws AbstractException {
        if (authService.isAdmin()) return taskRepository.getSize();
        return taskRepository.getSizeForUser(authService.getCurrentUserId());
    }

    @Override
    public void addTaskToProject(final String projectId, final String taskId) throws AbstractException {
        if (isEmptyString(projectId) || isEmptyString(taskId)) throw new IdEmptyException();
        if (authService.isAdmin()) addTaskToProjectForAdmin(projectId, taskId);
        else addTaskToProjectForUser(projectId, taskId, authService.getCurrentUserId());
    }

    @Override
    public void removeTaskFromProject(final String projectId, final String taskId) throws AbstractException {
        if (isEmptyString(projectId) || isEmptyString(taskId)) throw new IdEmptyException();
        if (authService.isAdmin()) removeTaskFromProjectForAdmin(projectId, taskId);
        else removeTaskFromProjectForUser(projectId, taskId, authService.getCurrentUserId());
    }

    @Override
    public List<Task> findAllTasksByProjectId(final String projectId, final Comparator<Task> taskComparator)
            throws AbstractException {
        if (isEmptyString(projectId)) throw new IdEmptyException();
        if (authService.isAdmin()) return findAllTasksByProjectIdForAdmin(projectId, taskComparator);
        return findAllTasksByProjectIdForUser(projectId, taskComparator, authService.getCurrentUserId());
    }

    @Override
    public Project removeProjectById(final String projectId) throws AbstractException {
        if (isEmptyString(projectId)) throw new IdEmptyException();
        if (authService.isAdmin()) return removeProjectByIdForAdmin(projectId);
        return removeProjectByIdForUser(projectId, authService.getCurrentUserId());
    }

    @Override
    public Project removeProjectByIndex(final int projectIndex) throws AbstractException {
        if (authService.isAdmin()) return removeProjectByIndexForAdmin(projectIndex);
        return removeProjectByIndexForUser(projectIndex, authService.getCurrentUserId());
    }

    @Override
    public Project removeProjectByName(final String projectName) throws AbstractException {
        if (isEmptyString(projectName)) throw new NameEmptyException();
        if (authService.isAdmin()) return removeProjectByNameForAdmin(projectName);
        return removeProjectByNameForUser(projectName, authService.getCurrentUserId());
    }

    @Override
    public void clearProjects() throws AbstractException {
        if (authService.isAdmin()) clearProjectsForAdmin();
        else clearProjectsForUser(authService.getCurrentUserId());
    }

}
