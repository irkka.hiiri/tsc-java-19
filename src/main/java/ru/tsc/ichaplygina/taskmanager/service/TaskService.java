package ru.tsc.ichaplygina.taskmanager.service;

import ru.tsc.ichaplygina.taskmanager.api.repository.IRepository;
import ru.tsc.ichaplygina.taskmanager.api.service.IAuthService;
import ru.tsc.ichaplygina.taskmanager.api.service.ITaskService;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.NameEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IndexIncorrectException;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.*;

public class TaskService extends AbstractEntityService<Task> implements ITaskService {

    public TaskService(IRepository<Task> repository, IAuthService authService) {
        super(repository, authService);
    }

    @Override
    public Task add(final String name, final String description) throws AbstractException {
        if (isEmptyString(name)) throw new NameEmptyException();
        final Task task = new Task(name, description, authService.getCurrentUserId());
        return super.add(task);
    }

    private Task removeByIndexForAdmin(final int index) throws AbstractException {
        if (isInvalidListIndex(index, repository.getSize())) throw new IndexIncorrectException(index + 1);
        return repository.removeByIndex(index);
    }

    private Task removeByIndexForUser(final int index, final String userId) throws AbstractException {
        if (isInvalidListIndex(index, repository.getSizeForUser(userId)))
            throw new IndexIncorrectException(index + 1);
        return repository.removeByIndexForUser(index, userId);
    }

    @Override
    public void clear() throws AbstractException {
        if (authService.isAdmin()) {
            repository.clear();
            return;
        }
        repository.clearForUser(authService.getCurrentUserId());
    }

    @Override
    public Task removeById(final String id) throws AbstractException {
        if (isEmptyString(id)) throw new IdEmptyException();
        if (authService.isAdmin()) return repository.removeById(id);
        return repository.removeByIdForUser(id, authService.getCurrentUserId());
    }

    @Override
    public Task removeByIndex(final int index) throws AbstractException {
        if (authService.isAdmin()) return removeByIndexForAdmin(index);
        return removeByIndexForUser(index, authService.getCurrentUserId());
    }

    @Override
    public Task removeByName(final String name) throws AbstractException {
        if (isEmptyString(name)) throw new NameEmptyException();
        if (authService.isAdmin()) return repository.removeByName(name);
        return repository.removeByNameForUser(name, authService.getCurrentUserId());
    }

}
