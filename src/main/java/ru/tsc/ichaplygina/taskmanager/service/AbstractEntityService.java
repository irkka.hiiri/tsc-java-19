package ru.tsc.ichaplygina.taskmanager.service;

import ru.tsc.ichaplygina.taskmanager.api.repository.IRepository;
import ru.tsc.ichaplygina.taskmanager.api.service.IAuthService;
import ru.tsc.ichaplygina.taskmanager.api.service.IService;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.NameEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.ProjectNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IndexIncorrectException;
import ru.tsc.ichaplygina.taskmanager.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;

import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isInvalidListIndex;

public abstract class AbstractEntityService<E extends AbstractBusinessEntity> implements IService<E> {

    protected final IRepository<E> repository;

    protected final IAuthService authService;

    public AbstractEntityService(final IRepository<E> repository, final IAuthService authService) {
        this.repository = repository;
        this.authService = authService;
    }

    private List<E> findAllForAdmin(final Comparator<E> comparator) {
        if (comparator == null) return repository.findAll();
        return repository.findAll(comparator);
    }

    private List<E> findAllForUser(final Comparator<E> comparator, final String userId) {
        if (comparator == null) return repository.findAllForUser(userId);
        return repository.findAllForUser(comparator, userId);
    }

    private E findByIndexForAdmin(final int index) throws AbstractException {
        if (isInvalidListIndex(index, repository.getSize())) throw new IndexIncorrectException(index + 1);
        return repository.findByIndex(index);
    }

    private E findByIndexForUser(final int index, final String userId) throws AbstractException {
        if (isInvalidListIndex(index, repository.getSizeForUser(userId)))
            throw new IndexIncorrectException(index + 1);
        return repository.findByIndexForUser(index, userId);
    }

    private void updateByIdForAdmin(final String id, final String name, final String description) throws AbstractException {
        if (repository.isNotFoundById(id)) throw new ProjectNotFoundException();
        repository.update(id, name, description);
    }

    private void updateByIdForUser(final String id, final String name, final String description, final String userId) throws AbstractException {
        if (repository.isNotFoundByIdForUser(id, userId)) throw new ProjectNotFoundException();
        repository.update(id, name, description);
    }

    private void updateByIndexForAdmin(final int index, final String name, final String description) throws AbstractException {
        if (isInvalidListIndex(index, repository.getSize())) throw new IndexIncorrectException(index + 1);
        final String id = repository.getId(index);
        if (repository.isNotFoundById(id)) throw new ProjectNotFoundException();
        updateById(id, name, description);
    }

    private void updateByIndexForUser(final int index, final String name, final String description, final String userId) throws AbstractException {
        if (isInvalidListIndex(index, repository.getSizeForUser(userId)))
            throw new IndexIncorrectException(index + 1);
        final String id = repository.getIdForUser(index, userId);
        if (repository.isNotFoundByIdForUser(id, userId)) throw new ProjectNotFoundException();
        updateById(id, name, description);
    }

    private void updateStatusForAdmin(final String id, final Status status) throws AbstractException {
        if (repository.isNotFoundById(id)) throw new ProjectNotFoundException();
        repository.updateStatus(id, status);
    }

    private void updateStatusForUser(final String id, final Status status, final String userId) throws AbstractException {
        if (repository.isNotFoundByIdForUser(id, userId)) throw new ProjectNotFoundException();
        repository.updateStatus(id, status);
    }

    @Override
    public boolean isEmpty() throws AbstractException {
        if (authService.isAdmin()) return repository.isEmpty();
        return repository.isEmptyForUser(authService.getCurrentUserId());
    }

    @Override
    public List<E> findAll(final Comparator<E> comparator) throws AbstractException {
        if (authService.isAdmin()) return findAllForAdmin(comparator);
        return findAllForUser(comparator, authService.getCurrentUserId());
    }

    @Override
    public E add(final E entity) throws AbstractException {
        repository.add(entity);
        return entity;
    }

    @Override
    public E findById(final String id) throws AbstractException {
        if (isEmptyString(id)) throw new IdEmptyException();
        if (authService.isAdmin()) return repository.findById(id);
        return repository.findByIdForUser(id, authService.getCurrentUserId());
    }

    @Override
    public E findByName(final String name) throws AbstractException {
        if (isEmptyString(name)) throw new NameEmptyException();
        if (authService.isAdmin()) return repository.findByName(name);
        return repository.findByNameForUser(name, authService.getCurrentUserId());
    }

    @Override
    public E findByIndex(final int index) throws AbstractException {
        if (authService.isAdmin()) return findByIndexForAdmin(index);
        return findByIndexForUser(index, authService.getCurrentUserId());
    }

    @Override
    public void updateById(final String id, final String name, final String description) throws AbstractException {
        if (isEmptyString(id)) throw new IdEmptyException();
        if (isEmptyString(name)) throw new NameEmptyException();
        if (authService.isAdmin()) updateByIdForAdmin(id, name, description);
        else updateByIdForUser(id, name, description, authService.getCurrentUserId());
    }

    @Override
    public void updateByIndex(final int index, final String name, final String description) throws AbstractException {
        if (authService.isAdmin()) updateByIndexForAdmin(index, name, description);
        else updateByIndexForUser(index, name, description, authService.getCurrentUserId());
    }


    @Override
    public void updateStatus(final String id, final Status status) throws AbstractException {
        if (isEmptyString(id)) throw new IdEmptyException();
        if (authService.isAdmin()) updateStatusForAdmin(id, status);
        else updateStatusForUser(id, status, authService.getCurrentUserId());
    }

    @Override
    public void startById(String id) throws AbstractException {
        if (isEmptyString(id)) throw new IdEmptyException();
        updateStatus(id, Status.IN_PROGRESS);
    }

    @Override
    public void startByIndex(int index) throws AbstractException {
        final boolean isAdmin = authService.isAdmin();
        final String userId = authService.getCurrentUserId();
        final int size = isAdmin ? repository.getSize() : repository.getSizeForUser(userId);
        if (isInvalidListIndex(index, size)) throw new IndexIncorrectException(index + 1);
        final String id = isAdmin ? repository.getId(index) : repository.getIdForUser(index, userId);
        updateStatus(id, Status.IN_PROGRESS);
    }

    @Override
    public void startByName(String name) throws AbstractException {
        if (isEmptyString(name)) throw new NameEmptyException();
        final String id = repository.getId(name);
        if (id == null) throw new ProjectNotFoundException();
        updateStatus(id, Status.IN_PROGRESS);
    }

    @Override
    public void completeById(String id) throws AbstractException {
        if (isEmptyString(id)) throw new IdEmptyException();
        updateStatus(id, Status.COMPLETED);
    }

    @Override
    public void completeByIndex(int index) throws AbstractException {
        final boolean isAdmin = authService.isAdmin();
        final String userId = authService.getCurrentUserId();
        final int size = isAdmin ? repository.getSize() : repository.getSizeForUser(userId);
        if (isInvalidListIndex(index, size)) throw new IndexIncorrectException(index + 1);
        final String id = isAdmin ? repository.getId(index) : repository.getIdForUser(index, userId);
        updateStatus(id, Status.COMPLETED);
    }

    @Override
    public void completeByName(String name) throws AbstractException {
        if (isEmptyString(name)) throw new NameEmptyException();
        final String id = repository.getId(name);
        if (id == null) throw new ProjectNotFoundException();
        updateStatus(id, Status.COMPLETED);
    }

    @Override
    public int getSize() throws AbstractException {
        if (authService.isAdmin()) return repository.getSize();
        return repository.getSizeForUser(authService.getCurrentUserId());
    }

}
