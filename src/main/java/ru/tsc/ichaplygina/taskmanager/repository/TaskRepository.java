package ru.tsc.ichaplygina.taskmanager.repository;

import ru.tsc.ichaplygina.taskmanager.api.repository.ITaskRepository;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TaskRepository extends AbstractEntityRepository<Task> implements ITaskRepository {

    @Override
    public void updateProjectId(final String taskId, final String projectId) {
        final Task task = findById(taskId);
        task.setProjectId(projectId);
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId) {
        List<Task> list = new ArrayList<>();
        for (Task task : super.list) {
            if (projectId.equals(task.getProjectId())) list.add(task);
        }
        return list;
    }

    @Override
    public List<Task> findAllByProjectIdForUser(final String projectId, final String userId) {
        List<Task> list = new ArrayList<>();
        for (Task task : findAllForUser(userId)) {
            if (projectId.equals(task.getProjectId())) list.add(task);
        }
        return list;
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId, Comparator<Task> comparator) {
        final List<Task> list = findAllByProjectId(projectId);
        list.sort(comparator);
        return list;
    }

    @Override
    public List<Task> findAllByProjectIdForUser(final String projectId, Comparator<Task> comparator, final String userId) {
        final List<Task> list = findAllByProjectIdForUser(projectId, userId);
        list.sort(comparator);
        return list;
    }

    @Override
    public boolean isNotFoundTaskInProject(final String taskId, final String projectId) {
        return (!findAllByProjectId(projectId).contains(findById(taskId)));
    }

    @Override
    public void removeAllByProjectId(final String projectId) {
        list.removeIf(task -> projectId.equals(task.getProjectId()));
    }

}
