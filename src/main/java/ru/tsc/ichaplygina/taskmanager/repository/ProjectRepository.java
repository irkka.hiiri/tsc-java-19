package ru.tsc.ichaplygina.taskmanager.repository;

import ru.tsc.ichaplygina.taskmanager.api.repository.IProjectRepository;
import ru.tsc.ichaplygina.taskmanager.model.Project;

public class ProjectRepository extends AbstractEntityRepository<Project> implements IProjectRepository {

}
