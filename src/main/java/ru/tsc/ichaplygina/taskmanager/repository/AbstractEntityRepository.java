package ru.tsc.ichaplygina.taskmanager.repository;

import ru.tsc.ichaplygina.taskmanager.api.repository.IRepository;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.model.AbstractBusinessEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class AbstractEntityRepository<E extends AbstractBusinessEntity> implements IRepository<E> {

    protected final List<E> list = new ArrayList<>();

    @Override
    public List<E> findAll() {
        return list;
    }

    @Override
    public List<E> findAll(Comparator<E> comparator) {
        List<E> list = findAll();
        list.sort(comparator);
        return list;
    }

    @Override
    public List<E> findAllForUser(final String userId) {
        final List<E> list = new ArrayList<>();
        for (E entity : this.list) {
            if (entity.getUserId().equals(userId)) list.add(entity);
        }
        return list;
    }

    @Override
    public List<E> findAllForUser(Comparator<E> comparator, final String userId) {
        final List<E> list = findAllForUser(userId);
        list.sort(comparator);
        return list;
    }

    @Override
    public int getSize() {
        return list.size();
    }

    @Override
    public int getSizeForUser(final String userId) {
        final List<E> list = findAllForUser(userId);
        return list.size();
    }

    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }

    @Override
    public boolean isEmptyForUser(final String userId) {
        final List<E> list = findAllForUser(userId);
        return list.isEmpty();
    }

    @Override
    public boolean isNotFoundById(final String id) {
        return findById(id) == null;
    }

    @Override
    public boolean isNotFoundByIdForUser(final String id, final String userId) {
        return findByIdForUser(id, userId) == null;
    }

    @Override
    public void add(final E entity) {
        list.add(entity);
    }

    @Override
    public void remove(final E entity) {
        list.remove(entity);
    }

    @Override
    public E update(final String id, final String name, final String description) {
        final E entity = findById(id);
        entity.setName(name);
        entity.setDescription(description);
        return entity;
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    public void clearForUser(final String currentUserId) {
        for (E entity : findAllForUser(currentUserId)) remove(entity);
    }

    @Override
    public E findById(final String id) {
        for (E entity : list) {
            if (id.equals(entity.getId())) return entity;
        }
        return null;
    }

    @Override
    public E findByIdForUser(final String id, final String userId) {
        for (E entity : list) {
            if (id.equals(entity.getId()) && userId.equals(entity.getUserId())) return entity;
        }
        return null;
    }

    @Override
    public E findByName(final String name) {
        for (E entity : list) {
            if (name.equals(entity.getName())) return entity;
        }
        return null;
    }

    @Override
    public E findByNameForUser(final String name, final String userId) {
        for (E entity : list) {
            if (name.equals(entity.getName()) && userId.equals(entity.getUserId())) return entity;
        }
        return null;
    }

    @Override
    public E findByIndex(final int index) {
        return list.get(index);
    }

    @Override
    public E findByIndexForUser(final int index, final String userId) {
        final List<E> list = findAllForUser(userId);
        return list.get(index);
    }

    @Override
    public E removeById(final String id) {
        final E entity = findById(id);
        if (entity == null) return null;
        this.remove(entity);
        return entity;
    }

    @Override
    public E removeByIdForUser(final String id, final String userId) {
        final E entity = findByIdForUser(id, userId);
        if (entity == null) return null;
        remove(entity);
        return entity;
    }

    @Override
    public E removeByIndex(final int index) {
        return list.remove(index);
    }

    @Override
    public E removeByIndexForUser(final int index, final String userId) {
        final E entity = findByIndexForUser(index, userId);
        if (entity == null) return null;
        remove(entity);
        return entity;
    }

    @Override
    public E removeByName(final String name) {
        final E entity = findByName(name);
        if (entity == null) return null;
        remove(entity);
        return entity;
    }

    @Override
    public E removeByNameForUser(final String name, final String userId) {
        final E entity = findByNameForUser(name, userId);
        if (entity == null) return null;
        remove(entity);
        return entity;
    }

    @Override
    public void updateStatus(final String id, final Status status) {
        final E entity = findById(id);
        entity.setStatus(status);
        if (status.equals(Status.IN_PROGRESS)) entity.setDateStart(new Date());
        else if (status.equals(Status.COMPLETED)) entity.setDateFinish(new Date());
    }

    @Override
    public String getId(final String name) {
        E entity = findByName(name);
        if (entity == null) return null;
        return entity.getId();
    }

    @Override
    public String getId(final int index) {
        E entity = findByIndex(index);
        if (entity == null) return null;
        return entity.getId();
    }

    @Override
    public String getIdForUser(final int index, final String userId) {
        E entity = findByIndexForUser(index, userId);
        if (entity == null) return null;
        return entity.getId();
    }

    @Override
    public String getIdForUser(final String name, final String userId) {
        E entity = findByNameForUser(name, userId);
        if (entity == null) return null;
        return entity.getId();
    }

}
