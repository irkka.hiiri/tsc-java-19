package ru.tsc.ichaplygina.taskmanager.constant;

public class StringConst {

    public static final String NEW_LINE = "\n";

    public static final String EMPTY = "";

    public static final String DELIMITER = " : ";

    public static final String PLACEHOLDER = "<empty>";

    public static final String COMMAND_PROMPT = "> ";

    public static final String ID_INPUT = "Please enter id: ";

    public static final String INDEX_INPUT = "Please enter index: ";

    public static final String NAME_INPUT = "Please enter name: ";

    public static final String DESCRIPTION_INPUT = "Please enter description: ";

    public static final String SORT_INPUT = "Sort output by: (optional) ";

    public static final String NO_TASKS_FOUND = "No tasks yet. Type <create task> to add a task.";

    public static final String NO_TASKS_FOUND_IN_PROJECT = "No tasks yet. Type <add task to project> to add a task to a project.";

    public static final String NO_PROJECTS_FOUND = "No projects yet. Type <create project> to add a project.";

    public static final String TASKS_CLEARED = " tasks removed.";

    public static final String TASK_ADDED_TO_PROJECT = "Task added to project.";

    public static final String TASK_CREATED = "Done. Type <list tasks> to view all tasks.";

    public static final String TASK_ID_INPUT = "Please enter task id: ";

    public static final String TASK_REMOVED = "Task removed.";

    public static final String TASK_REMOVED_FROM_PROJECT = "Task removed from project.";

    public static final String TASK_UPDATED = "Task updated.";

    public static final String PROJECTS_CLEARED = " projects removed.";

    public static final String PROJECT_CREATED = "Done. Type <list projects> to view all projects.";

    public static final String PROJECT_ID_INPUT = "Please enter project id: ";

    public static final String PROJECT_REMOVED = "Project removed.";

    public static final String PROJECT_UPDATED = "Project updated.";

    public static final String APP_COMMAND_SUCCESS = "Command completed successfully.";

    public static final String APP_COMMAND_ERROR = "An error has occurred.";

    public static final String APP_VERSION = "0.18.0";

    public static final String APP_WELCOME_TEXT = "*** WELCOME TO THE ULTIMATE TASK MANAGER ***" +
            NEW_LINE +
            NEW_LINE +
            "Enter <help> to show available commands." +
            NEW_LINE +
            "Enter <exit> to quit." +
            NEW_LINE +
            "Enter command: ";

    public static final String APP_HELP_HINT_TEXT = "Run with command-line arguments (as listed in [brackets])." +
            NEW_LINE +
            "Run with no arguments to enter command-line mode." +
            NEW_LINE +
            NEW_LINE +
            "Available commands and arguments:";

    public static final String APP_ABOUT = "Developed by:" +
            NEW_LINE +
            "Irina Chaplygina," +
            NEW_LINE +
            "Technoserv Consulting," +
            NEW_LINE +
            "ichaplygina@tsconsulting.com";

    public static final String SYSINFO_NO_LIMIT_TEXT = "No limit";

    public static final String SYSINFO_PROCESSORS = "Available processors: ";

    public static final String SYSINFO_FREE_MEMORY = "Free memory: ";

    public static final String SYSINFO_MAX_MEMORY = "Maximum memory: ";

    public static final String SYSINFO_TOTAL_MEMORY = "Total memory available to JVM: ";

    public static final String SYSINFO_USED_MEMORY = "Memory used by JVM: ";

}
