package ru.tsc.ichaplygina.taskmanager.api.service;

import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;

import java.util.List;
import java.util.Map;

public interface ICommandService {

    Map<String, AbstractCommand> getCommands();

    Map<String, AbstractCommand> getArguments();

    List<AbstractCommand> getCommandList();

}
