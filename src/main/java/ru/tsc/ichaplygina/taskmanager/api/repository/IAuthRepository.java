package ru.tsc.ichaplygina.taskmanager.api.repository;

public interface IAuthRepository {

    String getCurrentUserId();

    void setCurrentUserId(String currentUserId);

}
