package ru.tsc.ichaplygina.taskmanager.api.repository;

import ru.tsc.ichaplygina.taskmanager.model.User;

import java.util.List;

public interface IUserRepository {

    void add(final User user);

    List<User> findAll();

    boolean isEmpty();

    User findByLogin(final String login);

    User findById(String id);

    User findByEmail(String email);

    boolean isFoundByLogin(String login);

    boolean isFoundByEmail(String email);

    void remove(User user);

    void removeById(String id);

    void removeByLogin(String login);

    User update(String id, String login, String password, String firstName, String middleName, String lastName);
}
