package ru.tsc.ichaplygina.taskmanager.api;

import ru.tsc.ichaplygina.taskmanager.api.service.*;

public interface ServiceLocator {

    IProjectService getProjectService();

    ITaskService getTaskService();

    IProjectTaskService getProjectTaskService();

    ICommandService getCommandService();

    IUserService getUserService();

    IAuthService getAuthService();
}
