package ru.tsc.ichaplygina.taskmanager.api.service;

import ru.tsc.ichaplygina.taskmanager.model.Project;

public interface IProjectService extends IService<Project> {

}
