package ru.tsc.ichaplygina.taskmanager.api.repository;

import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;

public interface IRepository<E extends AbstractBusinessEntity> {
    List<E> findAll();

    List<E> findAllForUser(String userId);

    List<E> findAll(Comparator<E> comparator);

    List<E> findAllForUser(Comparator<E> comparator, String userId);

    int getSize();

    int getSizeForUser(String userId);

    boolean isEmpty();

    boolean isEmptyForUser(String userId);

    boolean isNotFoundById(String id);

    boolean isNotFoundByIdForUser(String id, String userId);

    void add(E entity);

    void remove(E entity);

    E update(String id, String name, String description);

    void clear();

    void clearForUser(String currentUserId);

    E findById(String id);

    E findByIdForUser(String id, String userId);

    E findByName(String name);

    E findByNameForUser(String name, String userId);

    E findByIndex(int index);

    E findByIndexForUser(int index, String userId);

    E removeById(String id);

    E removeByIdForUser(String id, String userId);

    E removeByIndex(int index);

    E removeByIndexForUser(int index, String userId);

    E removeByName(String name);

    E removeByNameForUser(String name, String userId);

    void updateStatus(String id, Status status);

    String getId(String name);

    String getId(int index);

    String getIdForUser(int index, String userId);

    String getIdForUser(String name, String userId);
}
