package ru.tsc.ichaplygina.taskmanager.api.repository;

import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IRepository<Task>{

    void updateProjectId(String taskId, String projectId);

    List<Task> findAllByProjectId(String projectId);

    List<Task> findAllByProjectIdForUser(String projectId, String userId);

    List<Task> findAllByProjectId(String projectId, Comparator<Task> comparator);

    List<Task> findAllByProjectIdForUser(String projectId, Comparator<Task> comparator, String userId);

    boolean isNotFoundTaskInProject(String taskId, String projectId);

    void removeAllByProjectId(final String projectId);

}
