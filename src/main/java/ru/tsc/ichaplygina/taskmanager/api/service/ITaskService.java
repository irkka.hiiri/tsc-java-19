package ru.tsc.ichaplygina.taskmanager.api.service;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.model.Task;

public interface ITaskService extends IService<Task> {

    void clear() throws AbstractException;

    Task removeById(String id) throws AbstractException;

    Task removeByIndex(int index) throws AbstractException;

    Task removeByName(String name) throws AbstractException;

}
