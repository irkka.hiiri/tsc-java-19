package ru.tsc.ichaplygina.taskmanager.api.service;

import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;

public interface IService<E extends AbstractBusinessEntity> {
    boolean isEmpty() throws AbstractException;

    List<E> findAll(Comparator<E> comparator) throws AbstractException;

    E add(String name, String description) throws AbstractException;

    E add(E entity) throws AbstractException;

    E findById(String id) throws AbstractException;

    E findByName(String name) throws AbstractException;

    E findByIndex(int index) throws AbstractException;

    void updateById(String id, String name, String description) throws AbstractException;

    void updateByIndex(int index, String name, String description) throws AbstractException;

    void updateStatus(String id, Status status) throws AbstractException;

    void startById(String id) throws AbstractException;

    void startByIndex(int index) throws AbstractException;

    void startByName(String name) throws AbstractException;

    void completeById(String id) throws AbstractException;

    void completeByIndex(int index) throws AbstractException;

    void completeByName(String name) throws AbstractException;

    int getSize() throws AbstractException;
}
