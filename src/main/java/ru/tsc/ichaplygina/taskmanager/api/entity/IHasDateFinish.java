package ru.tsc.ichaplygina.taskmanager.api.entity;

import java.util.Date;

public interface IHasDateFinish {

    Date getDateFinish();

    void setDateFinish(Date date);

}
