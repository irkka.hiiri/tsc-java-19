package ru.tsc.ichaplygina.taskmanager.api.service;

public interface ILogService {

    void info(final String message);

    void command(final String message);

    void error(final Exception e);

}
