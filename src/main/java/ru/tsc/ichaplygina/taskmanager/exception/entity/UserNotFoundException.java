package ru.tsc.ichaplygina.taskmanager.exception.entity;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

public class UserNotFoundException extends AbstractException {

    private static final String MESSAGE = "User not found.";

    public UserNotFoundException() {
        super(MESSAGE);
    }

}
