package ru.tsc.ichaplygina.taskmanager.exception.empty;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

public class LoginEmptyException extends AbstractException {

    private static final String MESSAGE = "Error! Login is empty.";

    public LoginEmptyException() {
        super(MESSAGE);
    }

}
