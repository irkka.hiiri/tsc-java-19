package ru.tsc.ichaplygina.taskmanager.exception.entity;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

public class UserExistsWIthEmailException extends AbstractException {

    private static final String MESSAGE = "User already exists with email: ";

    public UserExistsWIthEmailException(final String email) {
        super(MESSAGE + email);
    }

}
