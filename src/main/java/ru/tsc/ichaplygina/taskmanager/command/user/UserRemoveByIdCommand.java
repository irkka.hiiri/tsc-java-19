package ru.tsc.ichaplygina.taskmanager.command.user;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.exception.security.AccessDeniedException;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.ID_INPUT;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

public class UserRemoveByIdCommand extends AbstractUserCommand {

    private static final String NAME = "remove user by id";

    private static final String DESCRIPTION = "remove user by id";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws AbstractException {
        super.execute();
        if (!getAuthService().isAdmin()) throw new AccessDeniedException();
        final String id = readLine(ID_INPUT);
        getUserService().removeById(id);
    }

}
