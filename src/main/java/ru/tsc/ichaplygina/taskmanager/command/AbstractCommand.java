package ru.tsc.ichaplygina.taskmanager.command;

import ru.tsc.ichaplygina.taskmanager.api.ServiceLocator;
import ru.tsc.ichaplygina.taskmanager.api.service.IAuthService;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.exception.security.AccessDeniedNotAuthorizedException;

import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;

public abstract class AbstractCommand {

    protected ServiceLocator serviceLocator;

    protected IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract String getCommand();

    public abstract String getArgument();

    public abstract String getDescription();

    public void execute() throws AbstractException {
        if (getAuthService().isNoUserLoggedIn()) throw new AccessDeniedNotAuthorizedException();
    }

    @Override
    public String toString() {
        return (getCommand() + (isEmptyString(getArgument()) ? "" : " [" + getArgument() + "]") + " - " + getDescription() + ".");
    }

}
