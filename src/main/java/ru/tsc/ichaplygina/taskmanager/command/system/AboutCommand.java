package ru.tsc.ichaplygina.taskmanager.command.system;

import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class AboutCommand extends AbstractCommand {

    private final static String CMD_NAME = "about";

    private final static String ARG_NAME = "-a";

    private final static String DESCRIPTION = "show developer info";

    @Override
    public String getCommand() {
        return CMD_NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARG_NAME;
    }

    @Override
    public void execute() {
        printLinesWithEmptyLine(APP_ABOUT);
    }

}
