package ru.tsc.ichaplygina.taskmanager.command.task;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.INDEX_INPUT;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readNumber;

public class TaskCompleteByIndexCommand extends AbstractTaskCommand {

    private final static String NAME = "complete task by index";

    private final static String DESCRIPTION = "complete task by index";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws AbstractException {
        super.execute();
        final int index = readNumber(INDEX_INPUT);
        getTaskService().completeByIndex(index - 1);
        showUpdateResult();
    }

}
