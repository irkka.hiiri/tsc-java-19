package ru.tsc.ichaplygina.taskmanager.command.user;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.printLinesWithEmptyLine;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;

public class WhoAmICommand extends AbstractUserCommand {

    private static final String NAME = "whoami";

    private static final String DESCRIPTION = "print current user login";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws AbstractException {
        final String currentId = getAuthService().getCurrentUserId();
        if (isEmptyString(currentId)) {
            printLinesWithEmptyLine("No user currently logged in.");
            return;
        }
        printLinesWithEmptyLine(getUserService().findById(currentId));
    }

}
