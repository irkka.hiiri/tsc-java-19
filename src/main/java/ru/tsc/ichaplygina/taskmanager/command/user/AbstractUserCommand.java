package ru.tsc.ichaplygina.taskmanager.command.user;

import ru.tsc.ichaplygina.taskmanager.api.service.IUserService;
import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;

public abstract class AbstractUserCommand extends AbstractCommand {

    protected final String ENTER_LOGIN = "Enter login: ";

    protected final String ENTER_PASSWORD = "Enter password: ";

    protected final String ENTER_EMAIL = "Enter email: ";

    protected final String ENTER_ROLE = "Enter user role: (default: User) ";

    protected final String ENTER_FIRST_NAME = "Enter first name: ";

    protected final String ENTER_MIDDLE_NAME = "Enter middle name: ";

    protected final String ENTER_LAST_NAME = "Enter last name: ";

    public String getArgument() {
        return null;
    }

    protected IUserService getUserService() {
        return serviceLocator.getUserService();
    }

}
