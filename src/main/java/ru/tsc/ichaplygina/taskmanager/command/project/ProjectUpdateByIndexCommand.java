package ru.tsc.ichaplygina.taskmanager.command.project;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.ProjectNotFoundException;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    private final static String NAME = "update project by index";

    private final static String DESCRIPTION = "update project by index";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws AbstractException {
        super.execute();
        final int index = readNumber(INDEX_INPUT);
        if (getProjectService().findByIndex(index - 1) == null) throw new ProjectNotFoundException();
        final String name = readLine(NAME_INPUT);
        final String description = readLine(DESCRIPTION_INPUT);
        getProjectService().updateByIndex(index - 1, name, description);
        showUpdateResult();
    }

}
