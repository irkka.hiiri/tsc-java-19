package ru.tsc.ichaplygina.taskmanager.command.project;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class ProjectClearCommand extends AbstractProjectCommand {

    private final static String NAME = "clear projects";

    private final static String DESCRIPTION = "delete all projects";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws AbstractException {
        super.execute();
        final int count = getProjectTaskService().getProjectSize();
        getProjectTaskService().clearProjects();
        printLinesWithEmptyLine(count + PROJECTS_CLEARED);
    }

}
