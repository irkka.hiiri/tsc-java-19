package ru.tsc.ichaplygina.taskmanager.command.project;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class ProjectStartByIndexCommand extends AbstractProjectCommand {

    private final static String NAME = "start project by index";

    private final static String DESCRIPTION = "start project by index";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws AbstractException {
        super.execute();
        final int index = readNumber(INDEX_INPUT);
        getProjectService().startByIndex(index - 1);
        showUpdateResult();
    }

}
