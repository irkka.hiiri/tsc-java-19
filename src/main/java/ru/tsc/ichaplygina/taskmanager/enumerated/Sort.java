package ru.tsc.ichaplygina.taskmanager.enumerated;

import ru.tsc.ichaplygina.taskmanager.comparator.*;

import java.util.Comparator;

public enum Sort {

    CREATED("CREATED - Sort by created date", ComparatorByCreated.getInstance()),
    STARTED("STARTED - Sort by start date", ComparatorByDateStart.getInstance()),
    FINISHED("FINISHED - Sort by finish date", ComparatorByDateFinish.getInstance()),
    NAME("NAME - Sort by name", ComparatorByName.getInstance()),
    STATUS("STATUS - Sort by status", ComparatorByStatus.getInstance());

    private final String displayName;

    private final Comparator comparator;

    Sort(String displayName, Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Comparator getComparator() {
        return comparator;
    }

    @Override
    public String toString() {
        return displayName;
    }

}
