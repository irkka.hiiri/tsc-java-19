package ru.tsc.ichaplygina.taskmanager.util;

public class ValidationUtil {

    private ValidationUtil() {
    }

    public static boolean isEmptyString(final String string) {
        return string == null || string.isEmpty();
    }

    public static boolean isInvalidListIndex(final int index, final int size) {
        return (index < 0 || index >= size);
    }

}
